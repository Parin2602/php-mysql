<?php
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'router';
$queue = 'msgs';

$host = 'puffin.rmq2.cloudamqp.com';
$port = 5672;
$user = 'mykvpwcc';
$pass = 'heVp8DFT0ELqB4ipTEZWsi6EfVN2td6w';
$vhost = 'mykvpwcc';

$connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);

$channel = $connection->channel();
$channel->queue_declare($queue, false, true, false, false);


$channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, true, false);

$channel->queue_bind($queue, $exchange);

function process_message($message)
{
    
    $message_body = json_decode($message->body);
    $email = $message_body->email;
    $msg = 'Your problem is noted';
    mail($email,"My subject",$msg);
    // Send a message with the string "quit" to cancel the consumer.
    if ($message->body === 'quit') {
        $message->getChannel()->basic_cancel($message->getConsumerTag());
    }
}

$consumerTag = '';
$channel->basic_consume($queue, $consumerTag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $channel
 * @param \PhpAmqpLib\Connection\AbstractConnection $connection
 */
function shutdown($channel, $connection)
{
    $channel->close();
    $connection->close();
}

register_shutdown_function('shutdown', $channel, $connection);

// Loop as long as the channel has callbacks registered
while ($channel->is_consuming()) {
    $channel->wait();
}