<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Methods,Content-Type,Access-Control-Allow-Origin, Authorization');
// $data = json_decode(file_get_contents("php://input"),true);
// print_r($_POST);echo "<pre>";
require 'vendor/autoload.php';
use Aws\S3\S3Client;
$name = $_FILES["filename"]["name"];
$tmpName = $_FILES["filename"]["tmp_name"];
$type = $_FILES["filename"]["type"];
$size = $_FILES["filename"]["size"];
$errorMsg = $_FILES["filename"]["error"];
$explode = explode(".",$name);
$extension = end($explode);
$status = 1;
//starting PHP image upload error handlings
if(!$tmpName)
{
    $error =  "ERROR: Please choose file";
    $status = 0;
    
}
else if($size > 5242880)// if file size is larger than 5MB 
{
    $error = "ERROR: Please choose less than 5MB file for uploading";
    http_response_code(500);
    unlink($tmpName);
    $status = 0;
}
else if(!preg_match("/\.(gif|jpg|png|jpeg)$/i",$name)) 
{
    $error = "ERROR: Please choose the file only with the GIF, PNG or JPG file format";
    http_response_code(500);
    unlink($tmpName);
    $status = 0;
}
else if($errorMsg == 1)
{
    $error = "ERROR: An unexpected error occured while processing the file. Please try again.";
    http_response_code(500);
    $status = 0;
}

if($status != 0){
$name = preg_replace('/[^A-Za-z0-9]/', '-', $name);
$dest_file = "uploads/$name";
$bucket = $_POST['s3bucket'];
  
$s3Client = new S3Client([
    'version' => 'latest',
    'region' => 'ap-south-1',
    'credentials' => [
        'key'    => 'AKIASVNJJRUX5LD5R57L',
        'secret' => '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'
    ]
]);

$file_Path = $dest_file;
$key = basename($file_Path);
  
try {
    $result = $s3Client->putObject([
        'Bucket' => $bucket,
        'Key'    => $key,
        'Body'   => fopen($dest_file, 'r'),
        'ACL'    => 'public-read', // make file 'public'
    ]);

    http_response_code(200);
    $data['success'] = $name;
    // $data['status'] = 1;    
   
} catch (Aws\S3\Exception\S3Exception $e) {
    http_response_code(500);
    $error = "There was an error uploading the file.\n";
    $data['error'] = $error;
    // $data['status'] = 0; 
}
}else{
    $data['error'] = $error;
    // $data['status'] = 0; 
    http_response_code(500);
}

echo json_encode($data);
?>