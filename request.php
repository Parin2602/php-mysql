<?php
include_once 'Db.php';
include_once 'Customer.php';
$database = new Db();
$pdo = $database->checkConnection();
$customer = new Customer($pdo);
if($_SERVER['REQUEST_METHOD'] == 'GET'){
    $data = json_decode(file_get_contents("php://input"),true);
    // print_r($_GET);die;
      if(isset($_GET['requestid'])){
        $customer->requestid = $_GET['requestid'];
        $requestid = $customer->requestid;
        
        $res = $customer->get($requestid);
        if($res){
            $result['success'] =$res;
            echo json_encode($result);
        }else{
            $res['error']='error';
            $res['reason'] = 'No data found';
        }
        
      }else{
          $customer->priority = $data['priority'];
          $customer->pagenum = $data['pagenum'];
          $customer->page = $data['page'];
          $customer->assignee = $data['assignee'];
          $customer->requeststatus = $data['requeststatus'];
          $res = $customer->get();
          if($res){
            $result['success'] =$res;
            echo json_encode($result);
          }else{
                $res['error']='error';
                $res['reason'] = 'No data found';
          }
      }
} elseif($_SERVER['REQUEST_METHOD'] == 'POST') {
    $data = json_decode(file_get_contents("php://input"),true);
    // print_r($data);die;
    $customer->title = $data['title'];
    $customer->category = $data['category'];
    $customer->initiator = $data['initiator'];
    $customer->initiatoremail= $data['initiator_email'];
    $customer->priority= $data['priority'];
    $customer->assignee= $data['asignee'];
    $customer->message= $data['message'];
    if($customer->create()){
        $user_array = array(
            'success' => $customer->insert_id
        );
    }else{
        $user_array = array(
            'error' => 'error',
            'reason' => $customer->reason
        );
    }
    echo json_encode($user_array);
} elseif($_SERVER['REQUEST_METHOD'] == 'PUT') {
    $data = json_decode(file_get_contents("php://input"),true);
    $customer->requestid = $data['requestid'];
    $customer->title = $data['title'];
    $customer->category = $data['category'];
    $customer->initiator = $data['initiator'];
    $customer->initiatoremail= $data['initiator_email'];
    $customer->priority= $data['priority'];
    $customer->assignee= $data['asignee'];
    $customer->message= $data['message'];
    if($customer->update($customer->requestid)){
        $user_array = array(
            'success' => $customer->requestid
        );
    }else{
        $user_array = array(
            'error' => 'error',
            'reason' => $customer->reason
        );
    }
    echo json_encode($user_array);
} elseif($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $data = json_decode(file_get_contents("php://input"),true);
    
    if(isset($data['requestid'])){
      $customer->url = $data['requestid'];
      $res = $customer->delete( $customer->url);
      if($res){
        $user_data['success'] = $customer->url;
      }else{
        $user_data['error'] = 'error';
        $user_data['reason'] = 'No such record exists!';
      }
      echo json_encode($user_data);
    }  
}
?>