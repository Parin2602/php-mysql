<?php 
require_once "config.php";

$sql = "SELECT * FROM request";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $i=0;
    while($row = $result->fetch_assoc()){
         $select_data[$i] = $row;
         $i++;   
    }

    $data['list'] = $select_data;
    $data['status'] = 1;

} else{
    $data['list'] = '';
    $data['status'] = 0; 
}    
echo json_encode($data);
?>