<?php
$name = $_FILES["uploads"]["name"];
$tmpName = $_FILES["uploads"]["tmp_name"];
$type = $_FILES["uploads"]["type"];
$size = $_FILES["uploads"]["size"];
$errorMsg = $_FILES["uploads"]["error"];
$explode = explode(".",$name);
$extension = end($explode);
//starting PHP image upload error handlings
if(!$tmpName)
{
    echo "ERROR: Please choose file";
    exit();
}
else if($size > 5242880)// if file size is larger than 5MB 
{
    echo "ERROR: Please choose less than 5MB file for uploading";
    unlink($tmpName);
    exit();
}
else if(!preg_match("/\.(gif|jpg|png|jpeg)$/i",$name)) 
{
    echo "ERROR: Please choose the file only with the GIF, PNG or JPG file format";
    unlink($tmpName);
    exit();
}
else if($errorMsg == 1)
{
    echo "ERROR: An unexpected error occured while processing the file. Please try again.";
    exit();
}
// End of PHP image upload error handlings
$name = preg_replace('/[^A-Za-z0-9]/', '-', $name);
//Placing folder "uploads" where files will going to uploaded
$moveFile = move_uploaded_file($tmpName,"uploads/$name");

if($moveFile != true)
{
    echo "ERROR: File not uploaded. Please try again";
    unlink($tmpName);
    exit();
}
$uploadDir = 'uploads';
$moveToDir = 'uploads';
$new_height = 360;
$new_height ; '';

function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
{
    $path = $uploadDir . '/' . $image_name;
    
    $mime = getimagesize($path);

    if($mime['mime']=='image/png') { 
        $src_img = imagecreatefrompng($path);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $src_img = imagecreatefromjpeg($path);
    }   

    $old_x          =   imageSX($src_img);
    $old_y          =   imageSY($src_img);
    // echo "<pre>";print_r([$old_x,$old_y]);die;
    $new_width = $old_x;
    

    $thumb_h = $new_height;
    $thumb_w = ($new_height * $old_x)/$old_y;

    $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

    imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


    // New save location
    $new_thumb_loc = $moveToDir .'/new'. $image_name;
    // echo "<pre>";print_r($new_thumb_loc);die;
    if($mime['mime']=='image/png') {
        $result = imagepng($dst_img,$new_thumb_loc,8);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $result = imagejpeg($dst_img,$new_thumb_loc,80);
    }

    imagedestroy($dst_img); 
    imagedestroy($src_img);

    return $result;
}

$result =  createThumbnail($name,$new_width,$new_height,$uploadDir,$moveToDir);
echo $result;
echo "<h2>Original image:-</h2> ";
echo "<img src='uploads/$name' /> <br/>";
echo "<h2>Resized image:-</h2> ";
echo "<img src='uploads/new$name' />";

