<?php 
require_once "config.php";
require './vendor/autoload.php';
include 'rabbit-config.php';
Predis\Autoloader::register();
$redis = new Predis\Client();
// echo "<pre>"; print_r($_POST);die; 
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'router';
$queue = 'email';

$host = 'puffin.rmq2.cloudamqp.com';
$port = 5672;
$user = 'mykvpwcc';
$pass = 'heVp8DFT0ELqB4ipTEZWsi6EfVN2td6w';
$vhost = 'mykvpwcc';

$connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);

if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate name

    /* */
    /* [title] => a
    [category] => ss
    [initiator] => dd
    [priority] => HIGH
    [asignee] => dd
    [request_status] => CREATED */
    $title = trim($_POST["title"]);
    if(empty($title)){
        $error['title_err'] = "Please enter a title.";
        $status = 0;
    } elseif(!filter_var($title, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $error['title_err'] = "Please enter a valid name.";
        $status = 0;
    } else{
        $status = 1;
    }


    $category = trim($_POST['category']);
    if(empty($category)){
        $error['category_err'] = "Please enter a category.";
        $status = 0;
    } else{
        $status = 1;
    }


    $initiator = trim($_POST['initiator']);
    if(empty($initiator)){
        $error['initiator_err'] = "Please enter initiator's identity.";
        $status = 0;
    } else{
        $status = 1;
    }


    $initiator_email = trim($_POST["initiator_email"]);
    if(empty($initiator_email)){
        $error['initiator_email_err'] = "Please enter a email.";
        $status = 0;
    } elseif(!filter_var($initiator_email, FILTER_VALIDATE_EMAIL)){
        $error['initiator_email_err'] = "Please enter a valid email.";
        $status = 0;
    } else{
        $status = 1;
    }

    $priority = trim($_POST['priority']);
    if(empty($priority)){
        $error['priority_err'] = "Please enter priority";
        $status = 0;
    }  else{
        $status = 1;
    }

    $asignee = trim($_POST['asignee']);
    if(empty($asignee)){
        $error['asignee_err'] = "Please enter assignee's identity.";
        $status = 0;
    } else{
        $status = 1;
    }

    $request_status = trim($_POST['request_status']);
    if(empty($request_status)){
        $error['request_status_err'] = "Please select status";
        $status = 0;
    } else{
        $status = 1;
    }


    if(empty($error)) {
        // error_reporting(E_ALL);
        // ini_set('display_errors', 1);       
        $current_date = date('Y-m-d H:i:s');
        if($request_status == 'CLOSED'){
            $sql = "INSERT INTO request (title, category, initiator, initiatoremail, 
            assignee, priority, requeststatus, closed, created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            echo $conn->error;
            $stmt->bind_param("sssssssss", $title, $category, $initiator, $initiator_email, 
            $asignee, $priority, $request_status, $current_date, $current_date);
            $stmt->execute();
            $insert_id = $stmt->insert_id;
        
        }else{
            $sql = "INSERT INTO request (title, category, initiator, initiatoremail, 
            assignee, priority, requeststatus, created) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            echo $conn->error;
            $stmt->bind_param("ssssssss", $title, $category, $initiator, $initiator_email, 
            $asignee, $priority, $request_status, $current_date);
            $stmt->execute();
            $insert_id = $stmt->insert_id;
        }
        
        if($insert_id){
            $redis->set($insert_id,$_POST['message']);

            // 
            $channel = $connection->channel();

            $channel->queue_declare($queue, false, true, false, false);

            $channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, true, false);

            $channel->queue_bind($queue, $exchange);

            $messageBody = json_encode(['email' => $initiator_email,
                                        'title' => $title,
                                        'category' => $category,
                                        'initiator' => $initiator,
                                        'assignee' => $asignee ]);
            $message = new AMQPMessage($messageBody, array('content_type' => 'application/json', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
            $channel->basic_publish($message, $exchange);

            $channel->close();
            $connection->close();
            // 
        }

        $data['status'] = 1;
        $data['error'] = '';    
    }else{
        $data['status'] = 0;
        $data['error'] = $error;
    }

    echo json_encode($data);
}
    


?>