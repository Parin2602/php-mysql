<?php 
require_once "config.php";

if(isset($_POST['getid']) && $_POST['getid'] != ''){     

    $sql = "SELECT  * FROM request WHERE idrequest = '".$_POST['getid']."'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $data['list'] = $row;
    $data['status'] = 1;
    echo json_encode($data);
}


if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['type'])){
    $title = trim($_POST["title"]);
    if(empty($title)){
        $error['title_err'] = "Please enter a title.";
        $status = 0;
    } elseif(!filter_var($title, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $error['title_err'] = "Please enter a valid name.";
        $status = 0;
    } else{
        $status = 1;
    }


    $category = trim($_POST['category']);
    if(empty($category)){
        $error['category_err'] = "Please enter a category.";
        $status = 0;
    } else{
        $status = 1;
    }


    $initiator = trim($_POST['initiator']);
    if(empty($initiator)){
        $error['initiator_err'] = "Please enter initiator's identity.";
        $status = 0;
    } else{
        $status = 1;
    }


    $initiator_email = trim($_POST["initiator_email"]);
    if(empty($initiator_email)){
        $error['initiator_email_err'] = "Please enter a email.";
        $status = 0;
    } elseif(!filter_var($initiator_email, FILTER_VALIDATE_EMAIL)){
        $error['initiator_email_err'] = "Please enter a valid email.";
        $status = 0;
    } else{
        $status = 1;
    }

    $priority = trim($_POST['priority']);
    if(empty($priority)){
        $error['priority_err'] = "Please enter priority";
        $status = 0;
    }  else{
        $status = 1;
    }

    $asignee = trim($_POST['assignee']);
    if(empty($asignee)){
        $error['asignee_err'] = "Please enter assignee's identity.";
        $status = 0;
    } else{
        $status = 1;
    }

    $request_status = trim($_POST['request_status']);
    if(empty($request_status)){
        $error['request_status_err'] = "Please select status";
        $status = 0;
    } else{
        $status = 1;
    }

    $id = $_POST['id'];

    if(empty($error)) {
        // error_reporting(E_ALL);
        // ini_set('display_errors', 1);       
        $current_date = date('Y-m-d H:i:s');
        if($request_status == 'CLOSED'){
            $sql = "INSERT INTO request (title, category, initiator, initiatoremail, 
            assignee, priority, requeststatus, closed, created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            echo $conn->error;
            $stmt->bind_param("sssssssss", $title, $category, $initiator, $initiator_email, 
            $asignee, $priority, $request_status, $current_date, $current_date);
            $stmt->execute();       
            
            $sql = "UPDATE request SET title='".$title."',category= '".$category."',
                    initiator= '".$initiator."',initiatoremail= '".$initiator_email."',assignee= '".$asignee."',
                    priority= '".$priority."',requeststatus= '".$request_status."'
                    ,closed= '".$current_date."',category= '".$current_date."'  WHERE idrequest='$id'";

            if (mysqli_query($conn, $sql)) {
                $data['status'] = 1;
                $data['error'] = '';
            } else {
                $data['status'] = 2;
                $data['error'] = 'Update error';
            }
        }else{
            $sql = "UPDATE request SET title='".$title."',category= '".$category."',
                    initiator= '".$initiator."',initiatoremail= '".$initiator_email."',assignee= '".$asignee."',
                    priority= '".$priority."',requeststatus= '".$request_status."'
                    ,category= '".$current_date."'  WHERE idrequest='$id'";
            if (mysqli_query($conn, $sql)) {
                $data['status'] = 1;
                $data['error'] = '';
            } else {
                $data['status'] = 2;
                $data['error'] = 'Update error';
            }
        }
        
        $data['status'] = 1;
        $data['error'] = '';    
    }else{
        $data['status'] = 0;
        $data['error'] = $error;
    }

    echo json_encode($data);
}