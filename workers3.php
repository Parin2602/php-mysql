<?php
require_once __DIR__ . '/vendor/autoload.php';
use Aws\S3\S3Client;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'router';
$queue = 's3';

$host = 'puffin.rmq2.cloudamqp.com';
$port = 5672;
$user = 'mykvpwcc';
$pass = 'heVp8DFT0ELqB4ipTEZWsi6EfVN2td6w';
$vhost = 'mykvpwcc';

$connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);

$channel = $connection->channel();
$channel->queue_declare($queue, false, true, false, false);


$channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, true, false);

$channel->queue_bind($queue, $exchange);

function process_message($message)
{
    
    $message_body = json_decode($message->body);
    // echo "<pre>";print_r($message_body);die;
    $file = $message_body->file;
    $file_path= "uploads/".$file;
    $bucket = 'bucket2602';
  
    $s3Client = new S3Client([
    'version' => 'latest',
    'region' => 'ap-south-1',
    'credentials' => [
        'key'    => 'AKIASVNJJRUX5LD5R57L',
        'secret' => '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'
    ]
    ]);
    $key = basename($file_path);

    $response = $s3Client->doesObjectExist($bucket, $key);
    if($response){
        // $message->getChannel()->basic_cancel($message->getConsumerTag());
        echo "file already exist<br>";
    }else{
        try {
            $result = $s3Client->putObject([
                'Bucket' => $bucket,
                'Key'    => $key,
                'Body'   => fopen($file_path, 'r'),
                'ACL'    => 'public-read', // make file 'public'
            ]);
            echo "Image uploaded successfully. Image path is: ". $result->get('ObjectURL')."<br>";
        } catch (Aws\S3\Exception\S3Exception $e) {
            echo "There was an error uploading the file.\n";
            echo $e->getMessage();
        }
    }
    if ($message->body === 'quit') {
        $message->getChannel()->basic_cancel($message->getConsumerTag());
    }
}

$consumerTag = '';
$channel->basic_consume($queue, $consumerTag, false, false, false, false, 'process_message');

/**
 * @param \PhpAmqpLib\Channel\AMQPChannel $channel
 * @param \PhpAmqpLib\Connection\AbstractConnection $connection
 */
function shutdown($channel, $connection)
{
    $channel->close();
    $connection->close();
}

register_shutdown_function('shutdown', $channel, $connection);

// Loop as long as the channel has callbacks registered
while ($channel->is_consuming()) {
    $channel->wait();
}