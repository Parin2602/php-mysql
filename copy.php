<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Methods,Content-Type,Access-Control-Allow-Origin, Authorization');
$data = json_decode(file_get_contents("php://input"),true);

$folder_name = $data['folder'];
$filename = $data['filename'];
$bucket = $data['s3bucket'];
// print_r($folder_name);
require 'vendor/autoload.php';
use Aws\S3\S3Client;
require __DIR__ . '/vendor/autoload.php';

function getClient()
{   
    
    $client = new Google_Client();
    // $client->setRedirectUri("urn:ietf:wg:oauth:2.0:oob");
    $redirect_uri = 'http://localhost/php-mysql';
    $client->setRedirectUri($redirect_uri);
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes("https://www.googleapis.com/auth/drive");
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Drive($client);
$parameters['q'] = "mimeType='application/vnd.google-apps.folder' and name='$folder_name' and trashed=false";
    $files = $service->files->listFiles($parameters);
    $status = 1;
    $op = [];
    foreach( $files as $k => $file ){
        $op[] = $file;
    }

    if( count( $op ) == 0 ){
        http_response_code(500);
        $datas['success'] = 'Folder does not exist';
        $status = 0;
    }
    else{
        $folder_id = $op[0]['id'];
        $optParams = array(
            'pageSize' => 10,
            'fields' => 'nextPageToken, files(id, name)',
            'q' => "'".$folder_id."' in parents"
          );
          $results = $service->files->listFiles($optParams);
          
          if (count($results->getFiles()) == 0) {
            http_response_code(500); 
            $datas['success'] = 'Folder is empty';
            $status = 0;
          }else{
            $i = 0;  
            foreach ($results->getFiles() as $file) {
                $file_array[$i] = $file->getName();
                $file_id[$file->getName()] = $file->id;
                $i++;
            }
            
            $does_exist = array_search($filename,$file_array);
            
            if(isset($does_exist) && is_numeric($does_exist)) {
               
                $get_file_id  = $file_id[$file_array[$does_exist]];
                $content = $service->files->get($get_file_id, array("alt" => "media"));
                $outHandle = fopen("uploads/$filename", "w+");
                while (!$content->getBody()->eof()) {
                    fwrite($outHandle, $content->getBody()->read(1024));
                }
                    fclose($outHandle);
                    $dest_file = "uploads/$filename";
                    
                    $s3Client = new S3Client([
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => [
                            'key'    => 'AKIASVNJJRUX5LD5R57L',
                            'secret' => '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'
                        ]
                    ]);
                    
                    $file_Path = $dest_file;
                    $key = basename($file_Path);
                    
                    try {
                        $result = $s3Client->putObject([
                            'Bucket' => $bucket,
                            'Key'    => $key,
                            'Body'   => fopen($dest_file, 'r'),
                            'ACL'    => 'public-read', // make file 'public'
                        ]);
                        http_response_code(200);
                        $datas['success'] =  $filename;
                        $status = 1;        
                    } catch (Aws\S3\Exception\S3Exception $e) {
                        http_response_code(500);
                        $datas['success'] = "There was an error uploading the file.\n";
                        $status = 0;
                    }    
            } else {
                http_response_code(500);
                $datas['success'] = 'No such file exists';
                $status = 0;
            }
          }
    }

    echo json_encode($datas);


