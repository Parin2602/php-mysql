<?php 
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Methods,Content-Type,Access-Control-Allow-Origin, Authorization');
$email = $_POST['email'];
$name = $_FILES["filename"]["name"];
$tmpName = $_FILES["filename"]["tmp_name"];
$type = $_FILES["filename"]["type"];
$size = $_FILES["filename"]["size"];
$errorMsg = $_FILES["filename"]["error"];
$explode = explode(".",$name);

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'router';
$queue = 'email';

$host = 'puffin.rmq2.cloudamqp.com';
$port = 5672;
$user = 'mykvpwcc';
$pass = 'heVp8DFT0ELqB4ipTEZWsi6EfVN2td6w';
$vhost = 'mykvpwcc';

$connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);
// print_r($_FILES);

$extension = end($explode);
//starting PHP image upload error handlings
if(!$tmpName)
{
    // echo 21;
    $data['success'] = "ERROR: Please choose file";
    http_response_code(500);
    // exit();
}
else if($size > 5242880)// if file size is larger than 5MB 
{
    $data['success'] = "ERROR: Please choose less than 5MB file for uploading";
    http_response_code(500);
    // unlink($tmpName);
    // exit();             
}
else if(!preg_match("/\.(gif|jpg|png|jpeg)$/i",$name)) 
{
    $data['success'] = "ERROR: Please choose the file only with the GIF, PNG or JPG file format";
    http_response_code(500);
    unlink($tmpName);
    // exit();
}
else if($errorMsg == 1)
{
    
    $data['success'] = "ERROR: An unexpected error occured while processing the file. Please try again.";
    http_response_code(500);
    // exit();
}
// End of PHP image upload error handlings
$name = preg_replace('/[^A-Za-z0-9]/', '-', $name);

//Placing folder "uploads" where files will going to uploaded
$moveFile = move_uploaded_file($tmpName,"uploads/$name");
if($moveFile != true)
{
    $data['success'] = "ERROR: File not uploaded. Please try again";
    http_response_code(500);
    unlink($tmpName);
    
}
$uploadDir = 'uploads';
$moveToDir = 'uploads';
$new_height = 360;
$new_height ; '';

function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
{
    $path = $uploadDir . '/' . $image_name;
    
    $mime = getimagesize($path);

    if($mime['mime']=='image/png') { 
        $src_img = imagecreatefrompng($path);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $src_img = imagecreatefromjpeg($path);
    }   

    $old_x          =   imageSX($src_img);
    $old_y          =   imageSY($src_img);
    // echo "<pre>";print_r([$old_x,$old_y]);die;
    $new_width = $old_x;
    

    $thumb_h = $new_height;
    $thumb_w = ($new_height * $old_x)/$old_y;

    $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

    imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


    // New save location
    $new_thumb_loc = $moveToDir .'/new'. $image_name;
    // echo "<pre>";print_r($new_thumb_loc);die;
    if($mime['mime']=='image/png') {
        $result = imagepng($dst_img,$new_thumb_loc,8);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $result = imagejpeg($dst_img,$new_thumb_loc,80);
    }

    imagedestroy($dst_img); 
    imagedestroy($src_img);

    return $result;
}

$result =  createThumbnail($name,$new_width,$new_height,$uploadDir,$moveToDir);
if($result){
    $channel = $connection->channel();

    $channel->queue_declare($queue, false, true, false, false);

    $channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, true, false);

    $channel->queue_bind($queue, $exchange);

    $messageBody = json_encode(['email' => $email]);
    $message = new AMQPMessage($messageBody, array('content_type' => 'application/json', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
    $channel->basic_publish($message, $exchange);

    $channel->close();
    $connection->close();
    $status = 1;
    $data['success'] = $name;

}else{
    $status = 0;
    $data['success'] = 'Resizing failure';
}

echo json_encode($data);
?>
