<?php 
require_once "config.php";
require './vendor/autoload.php';
Predis\Autoloader::register();
$redis = new Predis\Client();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>PHP Training</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="ui.css">
</head>

<body>

    <div class="container">
        <h2>Customer Request</h2>
        <form id="customer_request" action="">
            <div class="row m-1">
                <div class="col">
                    <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="category" placeholder="Enter category" name="category">
                </div>
            </div>
            <div class="row m-1">
                <div class="col">
                    <input type="text" class="form-control" id="initiator" placeholder="Initiator" name="initiator">
                    <small class="initiator_err btn-danger"></small>
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="initiator_email" placeholder="Enter initiators email" name="initiator_email">
                    <small class="initiator_email_err btn-danger"></small>
                </div>
            </div>
            <div class="row m-1">
                <div class="col">
                    <input type="text" class="form-control" id="asignee" placeholder="Asignee" name="asignee">
                    <small class="asignee_err btn-danger"></small>
                </div>
                <div class="col">
                    <select id="priority" class="form-control" name="priority" required>
                        <option value="">Select priority</option>
                        <option value="HIGH">High</option>
                        <option value="NORMAL">Normal</option>
                        <option value="LOW">Low</option>
                    </select>
                    <small class="priority_err btn-danger"></small>
                </div>
            </div>
            <div class="row m-1">
                
                <div class="col">
                    <select class="form-control" id="request_status" name="request_status" required>
                        <option value="">Select status</option>
                        <option value="CREATED">CREATED</option>
                        <option value="ASSIGNED">ASSIGNED</option>
                        <option value="CLOSED">CLOSED</option>
                    </select>
                    <small class="request_status_err btn-danger"></small>
                </div>
            </div>
            <div class="row m-1">
                <div class="col">
                    <input type="text" class="form-control" id="message" placeholder="Customer Message" name="message">
                    <!-- <small class="asignee_err btn-danger"></small> -->
                </div>
            </div>    
            <button type="submit" class="btn btn-primary mt-3" id="doInsert">Submit</button>
            <button type="button" class="btn btn-primary mt-3 d-none" id="doUpdate">Update</button>
            <input type ="hidden" id="hidden_id">
        </form>
    </div>
    <div class="container-fluid m-3">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Category</th>
                <th scope="col">Initiator</th>
                <th scope="col">Assignee</th>
                <th scope="col">Status</th>
                <th scope="col">Priority</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
        
            <tbody class="appendData">
            
            </tbody>
        </table>
    </div>

    <table class="table">
         <thead>
             <tr>
             <th scope="col">Request ID</th>
             <th scope="col">Message</th>
             </tr>  
         </thead>  
         <tbody>
            
         <?php  
         $sql = "SELECT idrequest FROM request ORDER BY idrequest DESC";
         $result = $conn->query($sql);
            if($result->num_rows > 0) {
                $i=0;
                while($row = $result->fetch_assoc()){
                    echo "<tr>";
                    echo "<td>" . $row['idrequest'] . "</td>";
                    echo "<td>" . $redis->get($row['idrequest']) . "</td>";
                    echo "</tr>";
                }
            }      
         ?>
         
         </tbody> 
    </table>

    <script>

        function LoadData(){
                $('.appendData').empty();
                var append_data = '';
                $.ajax({
                type : 'POST',
                url : 'select_data.php',
                dataType : 'json',
                success : function(data){
                    console.log(data);
                    if(data.status){
                        $.each(data.list, function (key, val) {
                            console.log(key + '' + val.idrequest)
                            append_data = append_data + '<tr>';
                            append_data = append_data + '<th scope="row">'+val.idrequest+'</th><td>'+val.title+'</td><td>'+val.category+'</td><td>'+val.initiator+'</td>';
                            append_data = append_data + '<td>'+val.assignee+'</td><td>'+val.requeststatus+'</td><td>'+val.priority+'</td><td><button type="button" dataid="'+val.idrequest+'" class="btn btn-success update_btn">Update</button></td>';
                            append_data = append_data + '</tr>';
                        }); 
                        $('.appendData').append(append_data);       
                    }else{
                         
                    }
                },
                error : function(xhr){

                }

            }); 
        }    

        $(document).ready(function(){
            LoadData();
            $('#customer_request').on('submit',function(e){
                e.preventDefault();
                console.log( );
                $.ajax({
                    type : 'POST',
                    url : 'insert_data.php',
                    data : $( this ).serialize() ,
                    dataType : 'json',
                    success : function(data){
                        console.log(data);
                        if(data.status){
                             alert('Insertion successful');
                             window.location.reload();   
                        }else{
                            $.each(data.error, function (key, val) {
                                $('.'+ key).html(val);
                            });  
                        }
                    },
                    error : function(xhr){
 
                    }

                });
            });

            $('.appendData').on('click','.update_btn',function(){
                $('#hidden_id').val('');
                var id = $(this).attr('dataid');
                $.ajax({
                type : 'POST',
                url : 'update_data.php',
                dataType : 'json',
                data : {getid : id },
                success : function(data){
                    console.log(data)
                    $('#hidden_id').val(id);
                    $('#doInsert').hide();
                    $('#doUpdate').removeClass('d-none');
                    $('#title').val(data.list.title);
                    $('#category').val(data.list.category);
                    $('#initiator').val(data.list.initiator);
                    $('#initiator_email').val(data.list.initiatoremail);
                    $('#asignee').val(data.list.assignee);
                    $('#priority').val(data.list.priority);
                    $('#request_status').val(data.list.requeststatus);
                    
                }
                
                })    
            
            });


            $('#doUpdate').on('click',function(e){
                var title =  $('#title').val();
                var category = $('#category').val();
                var initiator = $('#initiator').val();
                var initiator_email = $('#initiator_email').val();
                var assignee = $('#asignee').val();
                var priority = $('#priority').val();
                var request_status = $('#request_status').val(); 
                var type = 'update';
                var id = $('#hidden_id').val();
                $.ajax({
                type : 'POST',
                url : 'update_data.php',
                dataType : 'json',
                data : {id:id, title : title,category:category,initiator:initiator, initiator_email:initiator_email,
                    assignee:assignee,priority:priority,request_status:request_status,type:type},
                success : function(data){
                    console.log(data);
                        if(data.status){
                             alert('Insertion successful');
                             window.location.reload();   
                        }else{
                            $.each(data.error, function (key, val) {
                                $('.'+ key).html(val);
                            });  
                        }
                }
                });    
            });


            

        });
    </script>

</body>

</html>