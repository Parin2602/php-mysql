<?php 
class Customer {
    private $pdo;
    private $table = 'request';
    public $title;
    public $category;
    public $initiator;
    public $initiatoremail;
    public $priority;
    public $assignee;
    public $message;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function create(){
        $this->reason = "";
        if(empty($this->title)){
            $this->reason = "Please enter a title.<br>";
            $status = 0;
            $this->error = 1;
        } elseif(!filter_var($this->title, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
            $this->reason = $this->reason. "Please enter a valid name.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }

        if(empty($this->category)){
            $this->reason = $this->reason. "Please enter a category.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }


        if(empty($this->initiator)){
            $this->reason = $this->reason. "Please enter initiator's identity.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }
        
        if(empty($this->initiatoremail)){
            $this->reason = $this->reason. "Please enter a email.<br>";
            $status = 0;
            $this->error = 1;
        } elseif(!filter_var($this->initiatoremail, FILTER_VALIDATE_EMAIL)){
            $this->reason = $this->reason. "Please enter a valid email.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }

        if(!$this->error) {

            error_reporting(E_ALL);
             ini_set('display_errors', 1); 
                $this->current_date = date('Y-m-d H:i:s');
                $sql = "INSERT INTO request (title, category, initiator, initiatoremail, 
                assignee, priority, created) VALUES (?, ?, ?, ?, ?, ?, ?)";
                $stmt = $this->pdo->prepare($sql);
                echo $this->pdo->error;
                $stmt->bind_param("sssssss", $this->title, $this->category, $this->initiator, $this->initiatoremail,$this->assignee, $this->priority, $this->current_date);
                $stmt->execute();
                
                $this->insert_id = $stmt->insert_id;
                if($this->insert_id){
                    http_response_code(200);
                    return true;
                }else{
                    http_response_code(200);
                    $this->reason = $this->reason.'Insertion not successful';
                    return false;
                }
            
        }else{
            http_response_code(406);
            return false;
        }
            
            
    }

    public function get($id=null){
        if($id){
            $sql = 'SELECT * FROM request WHERE idrequest ='.$id;
            // echo $sql;
            $result = $this->pdo->query($sql);
    
            if($result->num_rows > 0) {
                $i=0;
                while($row = $result->fetch_assoc()){
                    $this->select_data[$i] = $row;
                    $i++;   
                }
                http_response_code(200);
                return $this->select_data;
            }else{
                http_response_code(204);
                return false;
            }
        }else{

        
            $where = '';  
            $limit = '';  
            echo $this->priority;
        if(isset($this->priority)){
            $where = $where.' priority = "'.$this->priority.'" AND';
        }
        if(isset($this->assignee)){
            $where = $where.' assignee = "'.$this->assignee.'" AND';
        }
        if(isset($this->requeststatus)){
            $where = $where.' requeststatus = "'.$this->requeststatus.'" AND';
        }
        if(isset($this->pagenum) && isset($this->page)){
            $start = $this->pagenum * ($this->page-1);
            $end = $this->pagenum + $start;
            $limit = "LIMIT ".$start.",".$end; 
        }
        $additional_where = '(1=1)';
        $sql = 'SELECT * FROM request WHERE '.$where.' '.$additional_where.' '.$limit;
        // echo $sql;
        $result = $this->pdo->query($sql);

        if($result->num_rows > 0) {
            $i=0;
            while($row = $result->fetch_assoc()){
                $this->select_data[$i] = $row;
                $i++;   
            }
            http_response_code(200);
            return $this->select_data;
        }else{
            http_response_code(404);
            return false;
        }
        }      
    }

    public function delete($id){
        if(isset($id)){
            $sql = 'DELETE FROM request WHERE idrequest ='.$id;
            echo $sql;
            $result = $this->pdo->query($sql);
            //  echo $result; die;  
            if($result) {
                http_response_code(200);
                return true;
            }else{

                http_response_code(400);
                return false;
            }
        }
    }

    public function update($id){
        $this->reason = "";
        if(empty($this->title)){
            $this->reason = "Please enter a title.<br>";
            $status = 0;
            $this->error = 1;
        } elseif(!filter_var($this->title, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
            $this->reason = $this->reason. "Please enter a valid name.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }

        if(empty($this->category)){
            $this->reason = $this->reason. "Please enter a category.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }


        if(empty($this->initiator)){
            $this->reason = $this->reason. "Please enter initiator's identity.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }
        
        if(empty($this->initiatoremail)){
            $this->reason = $this->reason. "Please enter a email.<br>";
            $status = 0;
            $this->error = 1;
        } elseif(!filter_var($this->initiatoremail, FILTER_VALIDATE_EMAIL)){
            $this->reason = $this->reason. "Please enter a valid email.<br>";
            $status = 0;
            $this->error = 1;
        } else{
            $status = 1;
        }

        if(!$this->error) {
            error_reporting(E_ALL);
             ini_set('display_errors', 1); 
                $this->current_date = date('Y-m-d H:i:s');
                $sql = "UPDATE request SET title='".$this->title."',category= '".$this->category."',
                initiator= '".$this->initiator."',initiatoremail= '".$this->initiatoremail."',assignee= '".$this->assignee."',
                priority= '".$this->priority."',created= '".$this->current_date."'  WHERE idrequest='$id'";
        // echo $sql;die;
        if (mysqli_query($this->pdo, $sql)) {
            $data['success'] = $id;
            http_response_code(200);
            return true;
        } else {
            http_response_code(406);
            $this->reason = $this->reason. "Updation not successful";
            return false;
        }
            
        }else{
            http_response_code(404);
            return false;
        }
    }
    
} 
?>