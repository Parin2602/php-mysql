<!DOCTYPE html>
<html>

<head>
    <title>Send mail </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <hr>

        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <form role="form" method="post" action="#" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-9 form-group">
                            <label for="email">To Email:</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter sender email" maxlength="50">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-9 form-group">
                            <label for="subject">Subject:</label>
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject" maxlength="50">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-9 form-group">
                            <label for="name">Message:</label>
                            <textarea class="form-control" type="textarea" id="message" name="message" placeholder="Your Message Here" maxlength="6000" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-9 form-group">
                            <label for="name">Attachment:</label>
                            <input class="form-control" type="file" id="file" name="file">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9 form-group">
                            <button type="submit" name="sendmail" class="btn btn-lg btn-success btn-block">Send</button>
                        </div>
                    </div>
                </form>
                <?php 
		    require 'vendor/autoload.php';

            use Aws\Ses\SesClient;
            use Aws\Exception\AwsException;
            use PHPMailer\PHPMailer\PHPMailer;
            if(isset($_POST['sendmail'])){
                $SesClient = new SesClient([
                    // 'profile' => 'default',
                    'version' => '2010-12-01',
                    'region'  => 'ap-south-1',
                    
                ]);

                $sender_email = 'parinnagda@gmaill.com';

                $recipient_emails = [$_POST['email']];

                $subject = $_POST['subject'];
                $plaintext_body = $_POST['message'] ;
                
                $html_body =  '<h1>AWS Amazon Simple Email</h1>'.
                              '<p>'.$plaintext_body.'.</p>';
                $char_set = 'UTF-8';

    
    
                if(isset($_FILES['file'])){

                    // The full path to the file that will be attached to the email. 
                    $att = 'test.png';
                    
                    // Create a new PHPMailer object.
                    $mail = new PHPMailer;
                    // Add components to the email.
                    $mail->setFrom($sender_email, 'Mobiotics');
                    $mail->addAddress($recipient_emails[0]);
                    $mail->Subject = $subject;
                    $mail->Body = $html_body;
                    $mail->AltBody = $plaintext_body;
                    $mail->addAttachment($att);
//                    $mail->addCustomHeader('X-SES-CONFIGURATION-SET', $configset);

                    // Attempt to assemble the above components into a MIME message.
                    if (!$mail->preSend()) {
                        echo $mail->ErrorInfo;
                    } else {
                        // Create a new variable that contains the MIME message.
                        $message = $mail->getSentMIMEMessage();
                    }

                    // Try to send the message.
                    try {
                        $result = $SesClient->sendRawEmail([
                            'RawMessage' => [
                                'Data' => $message
                            ]
                        ]);
                        // If the message was sent, show the message ID.
                        $messageId = $result->get('MessageId');
                        echo("Email sent! Message ID: $messageId"."\n");
                    } catch (AwsException $error) {
                        // If the message was not sent, show a message explaining what went wrong.
                        echo("The email was not sent. Error message: "
                             .$error->getAwsErrorMessage()."\n");
                    }
                } else {

                    try {
                        $result = $SesClient->sendEmail([
                            'Destination' => [
                                'ToAddresses' => $recipient_emails,
                            ],
                            'ReplyToAddresses' => [$sender_email],
                            'Source' => $sender_email,
                            'Message' => [
                              'Body' => [
                                  'Html' => [
                                      'Charset' => $char_set,
                                      'Data' => $html_body,
                                  ],
                                  'Text' => [
                                      'Charset' => $char_set,
                                      'Data' => $plaintext_body,
                                  ],
                              ],
                              'Subject' => [
                                  'Charset' => $char_set,
                                  'Data' => $subject,
                              ],
                            ]
                        ]);
                        $messageId = $result['MessageId'];
                        echo("Email sent! Message ID: $messageId"."\n");
                    } catch (AwsException $e) {
                        // output error message if fails
                        echo $e->getMessage();
                        echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
                        echo "\n";
                    }
                }
            }
            ?>
            </div>
        </div>
    </div>
</body>

</html>