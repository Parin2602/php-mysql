<?php

require 'vendor/autoload.php';
use Aws\S3\S3Client;
function getClient()
{   
    
    $client = new Google_Client();
    // $client->setRedirectUri("urn:ietf:wg:oauth:2.0:oob");
    $redirect_uri = 'http://localhost/php-mysql';
    $client->setRedirectUri($redirect_uri);
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes("https://www.googleapis.com/auth/drive");
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}        
$client = getClient();
$service = new Google_Service_Drive($client);
$folderId = '1c1ZsieRGDNCiGC8DAcP-qsfALtNH_EfU';
        $name  = $_GET['name'];
        $id = $_GET['id'];
        // echo "<pre>";print_r($_GET);die;
        $content = $service->files->get($id, array("alt" => "media"));
        $outHandle = fopen("uploads/$name", "w+");
        while (!$content->getBody()->eof()) {
        fwrite($outHandle, $content->getBody()->read(1024));
        }

// Close output file handle.

fclose($outHandle);
echo "Done.\n";
$dest_file = "uploads/$name";
$bucket = 'bucket2602';
  
$s3Client = new S3Client([
    'version' => 'latest',
    'region' => 'ap-south-1',
    'credentials' => [
        'key'    => 'AKIASVNJJRUX5LD5R57L',
        'secret' => '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'
    ]
]);

$file_Path = $dest_file;
$key = basename($file_Path);
  
try {
    $result = $s3Client->putObject([
        'Bucket' => $bucket,
        'Key'    => $key,
        'Body'   => fopen($dest_file, 'r'),
        'ACL'    => 'public-read', // make file 'public'
    ]);
    echo "Image uploaded successfully. Image path is: ". $result->get('ObjectURL');
} catch (Aws\S3\Exception\S3Exception $e) {
    echo "There was an error uploading the file.\n";
    echo $e->getMessage();
}


?>